#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
FILE *fp;
struct BOOK {
	char name[256];
	char fio[256];
	int year;
};
int main() {
	fp = fopen("D:\\input.txt", "r");
	struct BOOK* book = (struct BOOK*)malloc(sizeof(struct BOOK));
	char s[256];
	int n = 0;
	while (fgets(s, 256, fp)) {
		++n;
		book = (struct BOOK*)realloc(book, n * sizeof(struct BOOK));
		book[n - 1].year = 0;
		int i = 0;
		int j = 0;
		while (s[i] != '\t') book[n - 1].name[j++] = s[i++];
		book[n - 1].name[j] = '\0';
		++i;
		j = 0;
		while (s[i]!='\t') book[n - 1].fio[j++] = s[i++];
		book[n - 1].fio[j] = '\0';
		++i;
		book[n - 1].year = 0;
		while (s[i] != '\n')
			book[n - 1].year = book[n - 1].year * 10 + s[i++] - '0';

	}
	for (int i = 0; i < n; ++i) printf("%s\t%s\t%i\n", book[i].name, book[i].fio, book[i].year);
	int newid = 0, oldid = 0;
	for (int i = 0; i < n; ++i) {
		if (book[i].year > book[newid].year)
			newid = i;
		if (book[i].year < book[oldid].year)
			oldid = i;
	}
	printf("The oldest book: %s\t%s\t%i\n", book[oldid].name, book[oldid].fio, book[oldid].year);
	printf("The newest book: %s\t%s\t%i\n", book[newid].name, book[newid].fio, book[newid].year);
	for (int i = 0; i < n; ++i) {
		int imin = i;
		for (int j = i + 1; j < n; ++j) {
			if (strcmp(book[j].fio, book[imin].fio) < 0) imin = j;
		}
		struct BOOK k = book[i];
		book[i] = book[imin];
		book[imin] = k;
	}
	for (int i = 0; i < n; ++i) printf("%s\t%s\t%i\n", book[i].name, book[i].fio, book[i].year);
	return 0;
	free(book);
}